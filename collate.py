#-*- coding: utf-8 -*-
#!/usr/bin/env python3

# Example usage:

# in_name = 'xd5.pdf'
# out_name = 'landscape_' + in_name
# in_path = Path(Path(__file__).parent, in_name).resolve()
# out_path = in_path.parent / out_name
# pdf = PdfFileReader(str(in_path))
# out = collate_pages(in_path)
# out = tile_pages(out, out_path=out_path)

from pathlib import Path
from math import floor, ceil
from pdfrw import PdfReader, PdfWriter, PageMerge
from PyPDF2 import PdfFileReader, PdfFileWriter
from PyPDF2.pdf import PageObject

def get_page_numbers(N):
    '''
    Returns list of pais of page numbers in a double-sided booklet layout.

    Args:
        N (int): number of pages
    Returns:
        numbers (list): list of pairs of 0-based page indices.
            None stands for a blank page.
    '''
    k = 2 * ceil(N/4) - 1
    d = 1
    numbers = []
    while k >= 0:
        first = k
        second = k+d if k+d < N else None
        numbers.append([first, second] if k % 2 == 1 else [second, first])
        k -= 1
        d += 2
    numbers = numbers[::-1]
    return numbers

def hstack_pages(left, right):
    ''' Stitches two pages horizontally.

    Args:
        left, right (PyPDF2.pdf.PageObject): left and right pages to merge.

    Returns:
        left (PyPDF2.pdf.PageObject): stitched pages.
    '''
    width = left.mediaBox[2] - left.mediaBox[0]
    left.mergeTranslatedPage(right, tx=width, ty=0.)
    left.mediaBox.upperRight = (left.mediaBox.getUpperRight_x() + right.mediaBox.getUpperRight_x(),
                                left.mediaBox.getUpperRight_y())
    left.cropBox.upperRight = (left.cropBox.getUpperRight_x() + right.cropBox.getUpperRight_x(),
                               left.cropBox.getUpperRight_y())
    return left

def vstack_pages(top, bottom):
    ''' Stitches two pages vertically.

    Args:
        top, bottom (PyPDF2.pdf.PageObject): top and bottom pages to merge.

    Returns:
        bottom (PyPDF2.pdf.PageObject): stitched pages.
    '''
    height = bottom.mediaBox[3] - bottom.mediaBox[1]
    bottom.mergeTranslatedPage(top, tx=0., ty=height)
    bottom.mediaBox.upperRight = (bottom.mediaBox.getUpperRight_x(),
                                  bottom.mediaBox.getUpperRight_y() + top.mediaBox.getUpperRight_y())
    bottom.cropBox.upperRight = (bottom.cropBox.getUpperRight_x(),
                                 bottom.cropBox.getUpperRight_y() + top.cropBox.getUpperRight_y())
    return bottom

def create_blank(page):
    ''' Returns a blank page with the size as input.

    Args:
        page (PyPDF2.pdf.PageObject): size equal to the output.

    Returns:
        blank (PyPDF2.pdf.PageObject): blank page.
    '''
    bbox = page.cropBox  # list-like ~ [x0, y0, x1, y1]
    width, height = bbox[2] - bbox[0], bbox[3] - bbox[1]
    blank = PageObject.createBlankPage(width=width, height=height)
    return blank

def collate_pages(pdf_path, out_path=None):
    ''' Stitches all pages of the document into a booklet.'''

    pdf = PdfFileReader(str(in_path))
    out = PdfFileWriter()
    blank = create_blank(pdf.pages[0])

    n = len(pdf.pages)
    page_numbers = get_page_numbers(n)
    for pair in page_numbers:
        left = pdf.pages[pair[0]] if pair[0] is not None else blank
        right = pdf.pages[pair[1]] if pair[1] is not None else blank
        stacked = hstack_pages(left, right)
        out.addPage(stacked)
    if out_path is not None:
        with open(str(out_path), 'wb') as out_file:
            out.write(out_file)
    return out

def tile_pages(pdf, out_path=None):
    ''' Tiles landscape A4 pages.
    '''
    n = pdf.getNumPages()
    out = PdfFileWriter()
    n_sheets = ceil(n/4.)
    for i in range(n_sheets):
        n_0 = i * 4
        # side 1
        top = pdf.getPage(n_0)  # first page of the fist side of the sheet alsways exists
        bottom = pdf.getPage(n_0 + 2) if (n_0 + 2) < n else create_blank(top)
        out.addPage(vstack_pages(top, bottom))
        # side 2
        top = pdf.getPage(n_0 + 1)  if (n_0 + 1) < n else create_blank(top)
        bottom = pdf.getPage(n_0 + 3) if (n_0 + 3) < n else create_blank(top)
        out.addPage(vstack_pages(top, bottom))
    if out_path is not None:
        with open(str(out_path), 'wb') as out_file:
            out.write(out_file)
    return out

def scale_pages(pdf, page_number, out_path):
    ''' Scales pages to match the size of the page with index <page_number>.'''
    bbox = pdf.pages[page_number].mediaBox
    width, height = float(bbox.getUpperRight_x()), float(bbox.getUpperRight_y())
    for i, page in enumerate(pdf.pages):
        pdf.pages[i].scaleTo(width=width, height=height)
        pdf.pages[i].cropBox.upperRight = bbox.upperRight
    out = PdfFileWriter()
    out.appendPagesFromReader(reader=pdf)
    with open(str(out_path), 'wb') as out_file:
       out.write(out_file)

